/*
    A simple gulp file just to lint my js and complie my Sass.
    I'd normally use the uglifier but I didn't think it was necessary for this.
*/

// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

var src = 'src/';
var build = 'build/';

// Lint Task
gulp.task('lint', function() {
    return gulp.src(src + 'js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src(src + 'styles/*.scss')
        .pipe(sass())
        .pipe(gulp.dest(build + 'css'));
});

// Concatenate & Minify JS
gulp.task('scripts', function() {
    return gulp.src(src + 'js/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest(build + 'js'));
});

// Move HTML files
gulp.task('html', function() {
    return gulp.src(src + '*.html')
        .pipe(gulp.dest(build));
});

// Move audio files
gulp.task('audio', function() {
    return gulp.src(src + 'audio/*')
        .pipe(gulp.dest(build + 'audio'));
});

// Move vendor files
// gulp.task('vendors', function() {
//     return gulp.src(src + 'vendors/**/*')
//         .pipe(gulp.dest(build + 'vendors'));
// });

// Move images (I normally compress them however I'm only using a couple here and they're small already.)
gulp.task('images', function() {
    return gulp.src(src + 'images/**/*')
        .pipe(gulp.dest(build + 'images'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch(src + 'js/*.js', ['lint', 'scripts']);
    gulp.watch(src + 'styles/*.scss', ['sass']);
    gulp.watch(src + '*.html', ['html']);
    gulp.watch(src + 'vendors/**/*', ['vendors']);
    gulp.watch(src + 'images/**/*', ['images']);
    gulp.watch(src + 'audio/*', ['audio']);
});

// Default Task
gulp.task('default', ['lint', 'sass', 'scripts', 'html', 'images', 'audio', 'watch']);