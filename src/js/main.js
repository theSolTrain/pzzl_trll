/*
	Javascript Module pattern, my fav.
*/
var pzzltrll = pzzltrll || {};

pzzltrll.game = (function(){
	'strict';

	var reverseX = false,
		reverseY = false;


	var main = {
		init: function() {
			// Only run if it is on the right page.
			if (document.getElementById('intro-screen')) {
				intro_screen.init();	
			} else if (document.getElementById('prize-page')) {
				prize_page.init();
			}
		}
	};


	var intro_screen = {
		init: function() {
			document.getElementById('trigger').addEventListener('click', function() {
				fade.in(document.getElementById('instructions'));
			});

			document.getElementsByClassName('intro-screen__button')[0].addEventListener('click', function() {
				fade.out(document.getElementById('intro-screen'));
				youtube.init();
			});
		}
	};


	var action = {
		onDragStart: function(e) {
			// console.log('onDragStart', e);
			e.dataTransfer.effectAllowed = 'move';
			e.dataTransfer.setData('text', e.target.id);
		},

		onDragEnd: function(e) {
			// console.log('onDragEnd', e);
			e.dataTransfer.clearData('data');
		},

		/*
			Here I'm checking that it is dragged over either the stage or the collection of puzzle pieces. 
			Without this check then it will drop pieces on top of other pieces and they will be destroyed.
		*/
		onDragOver: function(e) {
			if ((e.target.className == "puzzle__stage-slot") || (e.target.className == "puzzle__pieces")){
				e.preventDefault();
			} else {
				// Do nothing
			}
		},

		onDragEnter: function(e) {
			// Do nothing, but here in case I need it later.
		},

		drop: function(e) {
			// console.log('drop', e);
			var data = e.dataTransfer.getData('text');

			e.preventDefault();
			e.target.appendChild(document.getElementById(data));

			check_puzzle.video_treat(e);

			check_puzzle.check(e);
		}
	};


	var check_puzzle = {
		check: function(e) {
			var slots = document.getElementsByClassName('puzzle__stage-slot'),
				total_correct = [];

			/*
				Loop through each slot and look for a a puzzle piece.
				If there is a puzzle peice then check it's ID against the slot number.
				If it is correct then add it to the total_correct array.
			*/
			for (var i = 0; i < slots.length; i++) {
				if (slots[i].getElementsByClassName('piece')[0] !== undefined) {
					var piece = slots[i].getElementsByClassName('piece')[0],
						piece_id = piece.getAttribute('id').split('piece-0');

					if (parseInt(piece_id[1]) === (i + 1)) {
						// console.log('in the right place');
						total_correct.push(piece);
					} else {

					}
				} else {
					// console.log('Slot ' + (i + 1) + ' does not have a piece in it');
				}
			}

			/*
				Finally check if the total_correct array is the same length as the slots.
				Then fade out the pieces wrapper and stage. Finally fade in the stage and 
				call the animation. Wouldn't want this part to be easy...
			*/
			if (total_correct.length === slots.length) {
				fade.out(document.getElementsByClassName('puzzle__pieces')[0]);
				fade.out(document.getElementsByClassName('puzzle__stage')[0], function() {
					moving_target.init();
					fade.in(document.getElementsByClassName('puzzle__stage')[0]);
				});
			} else {
			}
		},

		video_treat: function(e) {
			var data = e.dataTransfer.getData('text'),
				dropped_piece_id = data.split('piece-0')[1],
				element = e.target,
				count = 0;

			// Loop through to the first element so I can work out it's position in the grid. 
			while(element.previousElementSibling) {
				count++;
				element = element.previousElementSibling;
			}

			/*
				Check the position of the piece in the grid against it's ID.
				If it matches then load a different video.
			*/

			if (parseInt(dropped_piece_id) === (count + 1))
				switch(count + 1) {
					case 1:
						youtube.loadVideo('zalYJacOhpo');
						break;

					case 2:
						youtube.loadVideo('KMFOVSWn0mI');
						break;

					case 3:
						youtube.loadVideo('kxopViU98Xo');
						break;

					case 4:
						youtube.loadVideo('F-S0T4xTdLY');
						break;

					case 5:
						youtube.loadVideo('O2ulyJuvU3Q');
						break;

					case 6:
						youtube.loadVideo('jI-kpVh6e1U');
						break;

					case 7:
						youtube.loadVideo('k0Ho9w70gsY');
						break;

					case 8:
						youtube.loadVideo('X18mUlDddCc');
						break;

					case 9:
						youtube.loadVideo('oHg5SJYRHA0');
						break;

					default:
						youtube.loadVideo('sCNrK-n68CM');
				}
		}
	};


	/*
		This is the Youtube API put into a module and broken down into methods. 
	*/
	var youtube = {
		player: window.player, // Fixing a scope issue I came across. 

		init: function() {
			youtube.loadAPI();
		},

		loadAPI: function() {
			var tag = document.createElement('script');

			tag.src = "https://www.youtube.com/iframe_api";
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

			window.onYouTubeIframeAPIReady = function() {
				youtube.player = new YT.Player('player', {
					videoId: 'sCNrK-n68CM',
					playerVars: {
						'autoplay': 1,
						'controls': 0,
						'disablekb': 1,
						'fs': 1,
						'iv_load_policy': 3,
						'rel': 0,
						'showinfo': 0
					},
					events: {
						'onReady': youtube.onPlayerReady,
						'onStateChange': youtube.onPlayerStateChange
					}
				});
			};
		},

		onPlayerReady: function(event) {
			event.target.playVideo();
		},

		onPlayerStateChange: function(event) {
			// console.log('State changed: ', event);
		},

		stopVideo: function() {
			youtube.player.stopVideo();
		},

		loadVideo: function(vid_id) {
			youtube.player.loadVideoById(vid_id);
		}
	};


	/*
		This is a fade in/fade out method to keep this a 'vanilla' javascript solution.
	*/
	var fade = {
		// Pass an element and a display type.
        in: function(element, display, callback) {
        	// Don't trigger the fade on repeated clicks.
        	if (element.style.opacity < 1) {
	            element.style.opacity = 0; // Make sure the element is not visible.
	            element.style.display = display || "block"; // Use the passed value or a default.

	            /*
					This self-executing, self-invoking function checks the value of the elements opacity
					and if it is les than 1 it will increment it. 
	            */
	            (function fade() {
	                var val = parseFloat(element.style.opacity);
	                if ((val += 0.1) <= 1) {
	                    element.style.opacity = val;
	                    /*
							This lets the browser know that we're performing an amination. 
							It draws at around 60fps and is executed at the next available time.
	                    */
	                    window.requestAnimationFrame(fade);
	                } else {
	                	if (typeof(callback) == 'function') {
	                		callback();
	                	}
	                }
	            })();
	        }
        },

        out: function(element, callback) {
        	/*
				This is the same as above but in reverse.
        	*/
            element.style.opacity = 1;

            (function fade() {
                if ((element.style.opacity -= 0.1) <= 0) {
                    element.style.display = "none";
                    if (typeof(callback) == 'function') {
                		callback();
                	}
                } else {
                    window.requestAnimationFrame(fade);
                }
            })();
        }
    };


    /*
		Here I am creating an animation loop to handle the movement of the stage after the puzzle is completed.
    */
    var moving_target = {
    	init: function() {
    		moving_target.update();
    	},

    	update: function() {
    		var element = document.getElementsByClassName('puzzle__stage')[0],
    			height = element.offsetHeight,
    			width = element.offsetWidth,
    			x = element.style.left.split('px')[0],
    			y = element.style.top.split('px')[0],
    			fps = 50,
    			rand = Math.floor(Math.random() * 10);

    		element.style.position = 'absolute';

    		/*
    			Here i'm checking that the x and y values aren't empty. 
    			I noticed this was an issue on the first loop.
    		*/
    		if (x === '') {
    			x ='0';
    		}

    		if (y === '') {
    			y ='0';
    		}

    		/*
    			Here i'm checking the current direction of the stage, then it's 
    			position against the viewport and deciding to reverse the direction or not.
    		*/
    		if (reverseX) {
    			if (parseInt(x) > 0) {
    				element.style.left = ((parseInt(x) - 20) + 'px');
    			} else {
    				reverseX = false;
    			}
    		} else {
    			if (parseInt(x) < (window.innerWidth - width)) {
    				element.style.left = ((parseInt(x) + 10) + 'px');
    			} else {
    				reverseX = true;
    			}
    		}

    		if (reverseY) {
    			if (parseInt(y) > 0) {
    				element.style.top = ((parseInt(y) - 20) + 'px');
    			} else {
    				reverseY = false;
    			}
    		} else {
    			if (parseInt(y) < (window.innerHeight - height)) {
    				element.style.top = ((parseInt(y) + 10) + 'px');
    			} else {
    				reverseY = true;
    			}
    		}

			moving_target.draw(fps);
    	},

    	draw: function(fps) {
    		setTimeout(function() {
				requestAnimationFrame(moving_target.init);
			}, 1000 / fps);
    	}
    };


    var prize_page = {
    	init: function() {
    		/*
    			Play audio when the trollface is clicked.
    		*/
    		document.getElementById('trollface').addEventListener('click', function() {
    			document.getElementsByTagName('audio')[0].load();
    			document.getElementsByTagName('audio')[0].play();
    		});
    	}
    };


	return {
		main : main.init,
		action : action,
		youtube: youtube
	};
}());

pzzltrll.game.main();