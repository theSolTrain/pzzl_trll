# Pzzl Trll #

A puzzle game to test your nerves. Sound IS needed.


### How do I play ###

* Drag a puzzle piece from the bottom into the stage above.
* Repeat until all the pieces are in the right place.
* Caputer the QR code image with a reader.


### How will I know I've won? ###

* The puzzle will not have an opacity (the colours will be solid).


### How do I get set up? ###

* Clone the repo.
* Make sure you have Gulp installed. [Details here](https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md)
* Open a terminal and navigate to the root folder.
* run 'npm install'.
* run 'gulp'.
* This will create a build folder.
* Now you can either open it directly or point to it through your webserver. 


### Future additions ###

* Randomise the pieces on load.
* Make a mobile friendly version, although Youtube videos on mobile don't autoplay.
* Create the following levels of the game.